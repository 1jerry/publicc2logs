##############################
###  admin (Roles, Users)
##############################
Meteor.publish 'roles', ->
  Meteor.roles.find()

Meteor.publish 'filteredUsers', (filter, org) ->
  filteredUserQuery @userId, filter, org

Meteor.publish 'userData', ->
  # allow logged in user to access their OrgId
  Meteor.users.find(
    _id: @userId
  ,
    fields:
      OrgId: 1
  )

##############################
###  countingPeriods (balance)
##############################
Meteor.publish 'countingPeriods', ->
  Collections.countingPeriods.find()

##############################
###  products (Drugs)
##############################
Meteor.publish 'products', ->
  Collections.products.find()

##############################
###  shipments
##############################
Meteor.publish 'shipments', ->
  Collections.shipments.find()


##############################
###  stores
##############################
Meteor.publish 'stores', ->
  query = Collections.stores.find()
  first = true
  keyfix = {}
  handle = query.observeChanges
    added: (id, obj) ->
      unless first
#        console.log "Add: ",id,obj.name,obj.number
        null
      unless obj.hasOwnProperty "number"
        keyfix[id] = 1
      return
  first = false
#  console.log "Set handle ",handle._id, _.keys handle._multiplexer._handles
  @ready()
  if _.keys(keyfix).length
    newnum = _.max(query.map (r) -> r.number or 0)
    _.keys(keyfix).forEach (id) ->
      newnum++
      Collections.stores.update id, {$set:{number:newnum}}
  @onStop ->
#    console.log "STOP handle ",handle._id
    handle.stop()
  query

##############################
###  transactions
##############################
Meteor.publish 'transactions', ->
  Collections.transactions.find()