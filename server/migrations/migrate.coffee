Migrations.add
  version: 1
  name: "Setup initial products, demo store, and users"  #from init
  up: ->
    console.log "appState=",App.appState
    # init products in servers/startup/init.coffee
    # init users in server/startup/loadUsers.js
    # init roles
    if Meteor.roles.find().count() < 2
      for role in "view receive log count Pharmacist admin demo".split " "
        Roles.createRole role
    # init DEMO stores
    demoStore = Collections.stores.findOne({number:0})
    if !demoStore
      demoStore = Collections.stores.insert
        number: 0
        name: "demo"
        company: "demo"
        city: "Portland"
        createdBy: "system"
        email: "test+demo-store@usd1.com"
    if App.appState is "dev"
      user = YAML.eval(Assets.getText('users.yml')).user_test
      user.username = demoStore+"*"+user.username
      email = user.email
      user.email = "0" + email
      id = Accounts.createUser user
      Meteor.users.update id,
        $set:
          OrgId: demoStore
      user.password = "demo"
      user.email = "1" + email
      user.username = demoStore+"*"+"demo"
      id = Accounts.createUser user
      Meteor.users.update id,
        $set:
          OrgId: demoStore
      console.log "Added users 'test' and 'demo' in demo store (dev instance)"
