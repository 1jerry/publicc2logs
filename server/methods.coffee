Meteor.methods
    sendEmail: (to, from, subject, text) ->
        check [to, from, subject, text], [String]
        @unblock()
        Email.send
            to: to
            from: from
            subject: subject
            html: text

    getIP: ->
        @connection.clientAddress

    isEmailUsed: (email) ->
        check email, String
        if Meteor.users.find("emails.address": email).count()
            return "notUnique"

    orgCheck: (company) ->
        if Meteor.user().username isnt "admin" and not Meteor.user().OrgId and company.number
            id = company._id
            Meteor.users.update Meteor.userId(),
                $set:
                    OrgId: id

#######
#  for users
######

    addUser: (options, store, roles, globalFlag) ->
        # Accounts.createUser
        #   no error - sentEnrollmentEmail
        check options, Schemas.user
        check store._id, String
        check roles, Match.OneOf(String,Array,undefined)
        check globalFlag, Match.OneOf(Boolean,undefined)
        bang = options.username.charAt() is "!"
        if bang and !Meteor.user().globalAdminUser
            throw new (Meteor.Error) ("Invalid user name")
        newUser =
            username: if bang then options.username.substr(1,99) else store._id+"*"+options.username
            email: options.email

        if !!options.name
            newUser.profile =
                name: options.name
        if Meteor.users.find(username: newUser.username).count()
            throw new (Meteor.Error)('username <b>' + options.username + '</b> already in use')
        id = Accounts.createUser newUser
        if !(bang and globalFlag)
            Meteor.users.update id,
                $set:
                    OrgId: store._id
        Roles.addUsersToRoles id, roles if roles
        unless process.env.IS_MIRROR
            Accounts.sendEnrollmentEmail id
            if options.username is 'admin'
                if process.env.MAIL_URL  # keep HTML from cluttering log
                    PrettyEmail.send 'basic',
                        to: options.email
                        subject: 'Your c2logs store has been created.'
                        heading: store.name + ' (' + store.company + ')'
                        headingSmall: 'Look for a separate email to set your password.'
                        message: "<i>Afterwards</i>, if you wish to register <i>other</i> browsers for this store,
                          click (or copy) this link:" + Meteor.absoluteUrl() + "stores/" + store._id
                else if Meteor.isServer
                    Email.send
                        to: options.email
                        from: "us@c2logs.com"
                        subject: 'Your c2logs store has been created.'
                        text: "<i>Afterwards</i>, if you wish to register <i>other</i> browsers for this store,
                          click (or copy) this link:" + Meteor.absoluteUrl() + "stores/" + store._id

        id

#######
#  for countingRecords
######

    countNow: (cid) ->
        # this will [re]count the current inventory and place the counts
        # in the current countingPeriods record (cid)
        localDbg = false
        countingRecord = Collections.countingPeriods.findOne cid
        return if not countingRecord
        activeDrugs = drugs().fetch()
        if not activeDrugs.length
            console.log "DEBUG: drugs() failed"
            return false
        activeIds = _.pluck activeDrugs, "_id"
        _.each countingRecord.items, (ele, idx) ->
            if drugRecord = Collections.products.findOne(ele.id)
                isActive = activeIds.indexOf ele.id
                if isActive > -1
                    activeIds[isActive] = ""
#                    console.log "DEBUG:",ele.id, "exists"
                else
                    console.log "DEBUG:", ele.id, "NO LONGER ACTIVE"
                balance = drugRecord.curQty()
                updateKey = "items." + idx + ".balance"
                console.log "DEBUG: exist-updateKey = %s", updateKey if localDbg
                updateObj = {}
                updateObj[updateKey] = balance
                if not _.has ele, 'pkgSize'
                    updateObj["items." + idx + ".pkgSize"] = drugRecord.pkgSize
                self = ele
                if _.every([
                        'balance'
                        'packageCount'
                        'extraCount'
                        'pkgSize'
                    ], (attr) ->
                    _.has self, attr
                )
                    balance = (self.packageCount * self.pkgSize) + self.extraCount - balance
                    updateObj["items." + idx + ".calcBalance"] = balance
                Collections.countingPeriods.update cid, {$set: updateObj}
        remainingIds = _.filter activeIds, (x) ->
            true if x
        if remainingIds.length
            console.log "DEBUG: NEW ids:", remainingIds if localDbg
            updateObj = {}
            idx = countingRecord.items.length
            for nextId in remainingIds
                if drug = Collections.products.findOne nextId
                    updateKey = "items." + idx++
                    console.log "DEBUG: new updateKey = %s", updateKey if localDbg
                    item =
                        id: drug._id
                        NDC: drug.NDC
                        pkgSize: drug.pkgSize
                        balance: drug.curQty()
                    updateObj[updateKey] = item
            console.log "DEBUG: ready to add object ", updateObj if localDbg
            id = Collections.countingPeriods.update cid, {$set: updateObj}, (error) ->
                if error
                    console.log error
        Collections.countingPeriods.update(
            cid,
            $push:
                counted:
                    at: new Date()
                    by: Meteor.user().username
        )
        true
    logCount: (cid, line, name, value) ->
        countingRecord = Collections.countingPeriods.findOne cid
        return if not countingRecord
        if not countingRecord.startedBy?
            Collections.countingPeriods.update(
                cid,
                $set:
                    startedBy: Meteor.user().username
                    startedAt: new Date()
            )
        updateObj = {}
        updateObj["items." + line + "." + name] = value
        Collections.countingPeriods.update(
            cid,
            $set: updateObj
        )
        countingRecord = Collections.countingPeriods.findOne cid
        self = countingRecord.items[line]
        if _.every([
                'balance'
                'packageCount'
                'extraCount'
                'pkgSize'
            ], (attr) ->
            _.has self, attr
        )
            balance = (self.packageCount * self.pkgSize) + self.extraCount - self.balance
            updateObj["items." + line + ".calcBalance"] = balance
            Collections.countingPeriods.update(
                cid,
                $set: updateObj
            )
        true
    logNote: (cid, line, name, value) ->
        countingRecord = Collections.countingPeriods.findOne cid
        return if not countingRecord
        updateObj = {}
        updateObj["items." + line + "." + name] = value
        Collections.countingPeriods.update(
            cid,
            $set: updateObj
        )
        true

    doneCounting: (cid) ->
        localDbg = false
        # finalize inventory counts for this period
        countingRecord = Collections.countingPeriods.findOne cid
        return if not countingRecord
        console.log "server:doneCounting: <start> '%s' #############################", cid if localDbg
        verifiedAt = countingRecord.verifiedAt
        if not verifiedAt
            verifiedAt = new Date()
            updateObj =
                verifiedBy: Meteor.user().username
                verifiedId: Meteor.userId()
                verifiedAt: verifiedAt
            Collections.countingPeriods.update(
                cid,
                $set: updateObj
            )
        #### update transactions
        countedAt = countingRecord.lastCounted()
        storeId = countingRecord.store
        query =
            storeId: storeId
            countedAt:
                $exists: false
            createdAt:
                $lt: countingRecord.lastCounted()
        update =
            countedAt: countedAt
        console.log "server:query = ", query if localDbg
        console.log "server:update = ", update if localDbg
        Collections.transactions.update query,
            $set:
                update
            ,
                multi: true
        console.log "server: updated!" if localDbg
        #### update last verified (counted) quantity
        query = {}
        for item in countingRecord.items
            drug = Collections.products.findOne(item.id)
            stores = drug.stores or {}
            stores[storeId] = item.balance + item.calcBalance
            update =
                $set:
                    stores:stores
            console.log "  products.update ",item.id, update if localDbg
            it = Collections.products.update item.id, update
            console.log "    result= ",it if localDbg
            # update transaction
            tquery =
                storeId: storeId
                NDC: item.NDC
                verifiedAt: verifiedAt
            if not Collections.transactions.findOne tquery
                newTrx =
                    NDC: item.NDC
                    qty: item.calcBalance
                    rx: countingRecord.title
                    trxDate: new Date()
                    type: 'add'
                    verifiedAt: verifiedAt
                    verifiedBy: Meteor.user().username
                    countedAt: countedAt
                    curqty: item.balance + item.calcBalance
                    comment: "Balanced"
                if item.calcBalance < 0
                    newTrx.type = 'remove'
                mdate = moment(newTrx.trxDate)
                newTrx.trxDate = mdate.add(mdate.zone(), "m").toDate() #adjust TZ
                Collections.transactions.insert newTrx
#                    validate: false
        #### all done
        Collections.countingPeriods.update(
            cid,
            $set:
                completedAt: new Date()
        )
        console.log "server:doneCounting: </end> ########" if localDbg
        true

drugs = ->
    query = {}
    key = "stores." + Meteor.user()?.profile?.activeStoreId
    query[key] = {$exists:true}
    result = Collections.products.find query, {sort:{desc:1}}
    result
