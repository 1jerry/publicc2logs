clearFiles =
    placeholderNotUsed:1
    addOnInit:1
#    products:1
#    transactions:1
importErrorCount = 0

Meteor.startup ->
    if "addOnInit" of clearFiles
        addFromSpreadsheet()
    # recheck master list every day
    Meteor.setInterval (addFromSpreadsheet), 24*60*60*1000
    if not process.env.MAIL_URL  # keep HTML from cluttering log
      Accounts.emailTemplates.enrollAccount.html = ->
        ""

    mss = Meteor.settings
    logIt = (test)->
        msg = "Starting a " + ( if test then "test version of a " else "") + "**"
        console.log msg + (mss.state or "dev") + "** instance.  " +
            if !mss then "no local settings found!)" else ""

    testing_flag = !!process?.env.c2logstest
    logIt testing_flag
    App.appState = mss.state or "dev"
    Migrations.migrateTo 'latest'

    Meteor.methods
        getServerEnv: ->
            msg = if testing_flag then "test version of a " else ""
            ## sets App on server only
            App.stateMessage = "This is a " + switch
                  when App.appState is 'live' then msg+"<b>live</b> system.  Be careful."
                  when App.appState is 'trial' then msg+"<b>trial</b> system.  All accounts
here have a limited life span."
                  else "<b>test</b> system.  All data here is transient."
            testState: testing_flag
            appState: App.appState
            stateMessage: App.stateMessage

#########################
  #
    #########################

convertDrugRecord = (rowCells) ->
    NDC = rowCells[1].value
    desc = rowCells[2].value
    form = rowCells[3].value
    pkgSize = parseInt rowCells[4].value
    supplier = rowCells[5].value
    # now process each field
    # desc
    desc = _(desc).strLeftBack " "  if _.any([
          "CAPSULE"
          "CAP"
          "CP"
          "PATCH"
          "TABLET"
          "TAB"
      ], (letters) ->
      _(desc).endsWith letters
    )
    desc = _(desc).titleize()
    # form
    form = form.toLowerCase()
    switch form
        when "cap" then form = "capsule"
        when "tab" then form = "tablet"
        when "unit" then form = "each"
    # supplier
    supplier = _(supplier).strLeftBack(" ").trimRight() if _(supplier).endsWith("@")
    supplier = _(supplier).titleize()
    # return object
    NDC: NDC
    desc: desc
    form: form
    pkgSize: pkgSize
    supplier: supplier

addFromSpreadsheet = ->
    if importErrorCount < 3
      try
        Meteor.call "spreadsheet/fetch", "1iNF8z0pNWtEmsgq-hBjhUcCVCZ5gF9L2fsv3zAN-7f0"
      catch error
        if ++importErrorCount >= 3
          console.log "********* Too many errors.  Stopped trying to import from Google Spreadsheet. ***********"
        throw error
      ssheet = GASpreadsheet.findOne()
      if ssheet
          _.each ssheet.cells, (rowCells, rowNum) ->
              if rowNum > 1
                  NDC = rowCells[1].value.replace(/[^0-9]/g ,"")  #remove any non-digits
                  unless Collections.products.findOne(NDC: NDC)
                      console.log "Add product %s", NDC
                      record = convertDrugRecord rowCells
                      Collections.products.insert record

# helpful patterns:
## process.stdout.write "Reloading transactions..."
