MochaWeb?.testOnly ->
  expect = chai.expect
  describe 'Products (drugs)', ->
    describe 'base', ->
      it 'should have > 100 records', (done) ->
        try
          expect Collections.products.find().count(), "no drugs?"
            .is.above 100
          done()
        catch e
          done e

