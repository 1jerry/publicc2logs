MochaWeb?.testOnly ->
  describe 'Stores', ->
    describe 'base', ->
      it 'should have one record', ->
        chai.assert.equal 1, Collections.stores.find().count()
      it 'should be named demo', ->
        chai.assert.equal "demo", Collections.stores.findOne().name
      it 'should be number 0', ->
        chai.assert.equal 0, Collections.stores.findOne().number

