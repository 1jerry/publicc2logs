if (!(typeof MochaWeb === 'undefined')) {
    MochaWeb.testOnly(function () {
        mocha.timeout(500);
        describe("Users ", function () {
            it("should have users", function () {
                chai.assert( Meteor.users)
            });
            it("should have only six users to start (4 if not dev)", function (done) {
                try {
                chai.assert.equal( Meteor.users.find().count(), 6);
                } catch (e) {done(e);}
            })
        });
        describe("User status functions", function () {
            var adminUser,
                testUser;
            before(function () {
                adminUser = Meteor.users.findOne({username: "admin"});
                testUser = Meteor.users.findOne({username: "test"});
            });
            it("user 'admin' is found ", function () {
                chai.assert.isTrue(
                    adminUser.username === 'admin'
                )});
            it("admin should be global", function () {
                chai.assert.isTrue(
                    adminUser.globalUser
                )});
            it("admin should be global & admin", function () {
                chai.assert.isTrue(
                    adminUser.globalAdminUser
                )});
            it("admin should have 'admin' role", function () {
                chai.assert(
                    Roles.userIsInRole(adminUser,['admin'])
                )});
            it("user 'test' is found ", function () {
                chai.assert.isTrue(
                    testUser.username === 'test'
                )});
            it("user 'test' should be global ", function () {
                chai.assert.isTrue(
                    testUser.globalUser
                )});
            it("user 'test' should NOT be admin global", function () {
                chai.assert.isFalse(
                    testUser.globalAdminUser
                )});
            it("user 'test' should have 'admin' role", function () {
                chai.assert.isTrue(
                    Roles.userIsInRole(testUser,['admin'])
                )});
        });
    });
}
