MochaWeb?.testOnly ->
  expect = chai.expect
  describe "Roles test", ->
    it "Roles collection should exist", ->
      expect Meteor.roles
    it "Seven Roles should have been created", (done) ->
      try
        expect Meteor.roles.find().count()
          .to.equal 7
        done()
      catch e
        done(e)
