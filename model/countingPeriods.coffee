Collections.countingPeriods = countingPeriods = new Mongo.Collection('countingPeriods')

Schemas.countingPeriods = new SimpleSchema(
  title:
    type: String
    optional: true

  counted:
    type: [Object]
    optional: true
  "counted.$.at":
    type: Date
    optional: true
  "counted.$.by":
    type: String
    optional: true

  verifiedAt:
    type: Date
    optional: true
  verifiedBy:
    type: String
    optional: true
  verifiedId:
    type: String
    optional: true
  completedAt:
    type: Date
    optional: true

  startedAt:
    type: Date
    optional: true
  startedBy:
    type: String
    optional: true

  store:
    type: String
    autoValue: ->
      if @isInsert
        value = Meteor.user().profile?.activeStoreId

  items:
    type: [Object]
    optional: true
  "items.$.id":
    type: String
  "items.$.NDC":
    type: String
#    denyUpdate: true  # from drugs (allow to add new items)
  "items.$.pkgSize":
    type: Number  # from drugs
#    denyUpdate: true  # from drugs (allow to update for older records)
  "items.$.balance":
    type: Number
    optional: true
  "items.$.packageCount":
    type: Number
    optional: true
  "items.$.extraCount":
    type: Number
    optional: true
  "items.$.calcBalance":
    type: Number
    optional: true
  "items.$.note":
    type: String
    optional: true
)

countingPeriods.attachSchema Schemas.countingPeriods

# Collection2 already does schema checking
# Add custom permission rules if needed
countingPeriods.allow(
  insert : -> true

  update : -> true

  remove : -> true
)
