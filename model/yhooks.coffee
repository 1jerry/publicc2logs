ignoreList = []
#################
# common to all files
#################
getUser = ->
  # handles first time run where no user exists yet
  try
    Meteor.user().username
  catch _error
    "system"

for name in _.keys Collections
  Collections[name].before.insert (userId, doc) ->
    doc.createdAt = new Date
    doc.createdBy = getUser()
    doc.createdId = userId

Collections.products.before.insert (userId, doc) ->
  if Meteor.isClient
    doc.storeNumber = Meteor.user?().store().number or 0

#noinspection CoffeeScriptUnusedLocalSymbols
Collections.products.after.insert (userId, doc) ->
  if Meteor.isClient
    $("td.NDC").mask("00000-0000-00")  # to show mask on newly added line

Collections.stores.before.insert (userId, doc) ->
  if Meteor.isServer
    if doc.number is undefined
      newnum = _.max(Collections.stores.find().map (r) -> r.number or 0)
      newnum = 0 if newnum < 0 or isNaN newnum
      doc.number = ++newnum
    options =
      username: 'admin'
      email: doc.email
      name: ''
    console.log "Adding admin user for store ",doc._id,options
    Meteor.call "addUser", options, doc, "admin", (e) ->
      if e
        console.log "stores/stores.coffee after.insert error: ",e

#################
  # collection helpers
#################
Collections.countingPeriods.helpers
  lastCounted: ->
    @counted[@counted.length-1]['at'] if @counted and @counted.length
  lastCountedBy: ->
    @counted[@counted.length-1]['by'] if @counted and @counted.length


Meteor.users._transform = (user) ->
  user.globalAdminUser = user.username is "admin"
  user.globalUser = !user.OrgId
  user.adminUser = 'admin' in (user.roles or [])
  user.mobileUser = user.username and !("*" in user.username) and !user.globalUser
  user.store =  ->
    Collections.stores.findOne
      _id: user.profile.activeStoreId or user.OrgId
  user.username = user.username.split("*",2)[1] or user.username if user.username
  user

#################
# user schema
#################
Schemas.user = new SimpleSchema({
  username: {
    type: String,
    regEx: /^!?[a-z0-9A-Z_]{3,15}$/
  },
  "email": {
    type: String,
    regEx: [SimpleSchema.RegEx.Email, /\.[a-zA-Z0-9]{2,15}$/]
  },
  "name": {
    type: String,
    optional: true
  }
});
Schemas.user.messages({"regEx username": [
  {
    exp: /^[a-z0-9A-Z_]{3,15}$/,
    msg: "Use 3-15 letters or numbers or _"
  }
]});

