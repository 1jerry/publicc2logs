Newdoc = (doc) ->
  _.extend this, doc
  return

_.extend Newdoc::,
  curQty: ->
    storeId = Meteor.user()?.profile?.activeStoreId
    return 0 if not storeId
    trxs = Collections.transactions.find(
      NDC: @NDC
      storeId: storeId
      countedAt:
        $exists:false
    )
    if @stores
      lastQty =  @stores?[storeId] or 0
      lastQty + _.reduce(trxs.fetch(), (o, n) ->
        o + n.qty
      , 0)
    else 0
  active: ->
    Meteor.user()?.profile?.activeStoreId in _.keys(@stores or {})
  creator: ->
    Meteor.users.findOne
      _id: @createdId
Collections.products = products = new Mongo.Collection("products",
  transform: (doc) ->
    new Newdoc(doc)
)
Schemas.drug = new SimpleSchema(
  NDC:
    type: String
    max: 13
    min: 9
    index: 1
    unique: true
    autoValue: ->
      if @isInsert
        res = @value?.replace(/[^0-9]/g ,"")  #remove any non-digits
        if res?.length is 9
          res += "00"
        res

  desc:
    type: String

  dose:
    type: String
    optional: true

  form:
    type: String
    defaultValue: 'tablet'
    allowedValues: [
      'capsule'
      'each'
      'ounce'
      'ml'
      'patch'
      'tablet'
    ]
    autoform:
      options: [
        {
          label: 'Capsule'
          value: 'capsule'
        }
        {
          label: 'Each'
          value: 'each'
        }
        {
          label: 'Ounce'
          value: 'ounce'
        }
        {
          label: 'MLs'
          value: 'ml'
        }
        {
          label: 'Patch'
          value: 'patch'
        }
        {
          label: 'Tablet'
          value: 'tablet'
        }
      ]

  pkgSize:
    type: Number
    defaultValue: 100
    min: 1
    label: "Package Size"

  supplier:
    type: String

  storeNumber:
    type: Number
    optional: true

  stores:
    type: Object
    optional: true
    blackbox: true

#  stores.key:
#    type: Number
#    defaultValue: 0
)
products.attachSchema Schemas.drug

#
# * Add query methods like this:
# *  Products.findPublic = function () {
# *    return Products.find({is_public: true});
# *  }
# 


# Collection2 already does schema checking
# Add custom permission rules if needed
if Meteor.isServer
  products.allow
    insert: ->
      true

    update: ->
      true

    remove: ->
      true
