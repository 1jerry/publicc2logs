Collections.shipments = shipments = new Mongo.Collection('shipments')

Schemas.shipment = new SimpleSchema(
  invoice:
    type: String

  company:
    type: String

  finalized:
    type: Boolean
    optional: true

  verifiedAt:
    type: Date
    optional: true

  verifiedBy:
    type: String
    optional: true

  store:
    type: String
    autoValue: ->
      if @isInsert
        value = Meteor.user().profile?.activeStoreId

  items:
    type: [Object]
    optional: true
  "items.$.id":
    type: String
  "items.$.NDC":
    type: String
  "items.$.quantity":
    type: Number
)

shipments.attachSchema Schemas.shipment

# Collection2 already does schema checking
# Add custom permission rules if needed
shipments.allow(
  insert : -> true

  update : -> true

  remove : -> true
)
