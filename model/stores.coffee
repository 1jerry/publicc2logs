Collections.stores = stores = new Mongo.Collection('stores')

Schemas.store = new SimpleSchema(
  number:
    type: Number
    optional: true
    autoform:
      omit: true

  name:
    type: String

  company:
    type: String
    optional: true

  city:
    type: String

  state:
    type: String
    defaultValue: "OR"
    allowedValues: [
      "OR"
      "WA"
    ]
  email:
    type: String,
    regEx: [SimpleSchema.RegEx.Email, /\.[a-zA-Z0-9]{2,15}$/]
    custom: ->
      if Meteor.isClient and @isSet
        Meteor.call "isEmailUsed", @value, (e,used) ->
          if used
            Schemas.store.namedContext "addStoreForm"
              .addInvalidKeys [
                name: "email"
                type: used
                ]
      if Meteor.isServer and @isSet
        Meteor.call "isEmailUsed", @value

  IPcreatedFrom:
    type: String
    denyUpdate: true
    optional: true
    autoform:
      omit: true

  createdAt:
    type: Date,
    denyUpdate: true
    optional: true
    autoform:
      omit: true

)

stores.attachSchema Schemas.store

# Collection2 already does schema checking
# Add custom permission rules if needed
stores.allow(
  insert : -> true

  update : -> true

  remove : -> true
)
