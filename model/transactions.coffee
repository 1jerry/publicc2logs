SimpleSchema.debug = true;
default2now = ->

    #  timestamp for Schema
    if @isInsert
        new Date
    else
        @unset()

Collections.transactions = transactions = new Meteor.Collection("transactions")

#noinspection JSUnusedGlobalSymbols
Schemas.transactions = new SimpleSchema(
    NDC:
        type: String
        autoform:
            omit: true

    comment:
        type: String
        optional: true

    storeId:
        type: String
        autoValue: ->
            if @isInsert
                #noinspection CoffeeScriptUnusedLocalSymbols
                value = Meteor.user().profile?.activeStoreId
    trxDate:
        type: Date
#        max: moment(
#            h: 23
#            m: 59
#            s: 59
#        ).toDate()
        min: moment().add(-30, "d").toDate()

    verifiedAt:
        type: Date
        optional: true
        autoform:
            omit: true

    rx:
        type: String
        autoform:
            label: "Rx or order number"

    type:
        type: String
        allowedValues: [
            'add'
            'dispense'
            'remove'
        ]
        defaultValue: 'dispense'
        autoform:
            options: [
                {
                    label: 'Add'
                    value: 'add'
                }
                {
                    label: 'Dispense'
                    value: 'dispense'
                }
                {
                    label: 'Remove'
                    value: 'remove'
                }
            ]
    qty:
        type: Number
        autoform:
            label: "Quantity"
            min: 1
        custom: ->
            maxqty = undefined
            NDC = @field("NDC").value
            if typeof (currentOrder) isnt "undefined" and currentOrder.get()
                maxqty = currentOrder.get().curQty()
                ##console.log "Validate NDC %s, maxqty from currentOrder=",NDC,maxqty
            else
                # from server?
                if @countedAt
                    maxqty = 99999999
                    #allow balancing record
                else
                    storeId = Meteor.user().profile?.activeStoreId
                    trxs = transactions.find(
                        NDC: NDC
                        storeId: storeId
                        countedAt:
                            $exists:false
                    )
                    changes = _.reduce(trxs.fetch(), (o, n) ->
                        o + n.qty
                    , 0)
                    drug = Collections.products.findOne(NDC: NDC)
                    maxqty = if storeId and drug.stores then drug.stores[storeId] else 0
                    maxqty += changes
                    ##console.log "Validate NDC %s, store %s, maxqty, changes=",NDC,storeId,maxqty,changes
            "maxNumber"  if @value + maxqty <0

    curqty:
        type: Number
        optional: true

    dispensed:
        type: Boolean
        optional: true

    verifiedBy:
        type: String
        optional: true

    countedAt:
        type: Date
        optional: true
        autoform:
            omit: true

)
transactions.attachSchema Schemas.transactions
# Collection2 already does schema checking
# Add custom permission rules if needed
if Meteor.isServer
    transactions.allow
        insert: ->
            true

        update: ->
            true

        remove: ->
            true
