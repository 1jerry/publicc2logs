# c2logs - Permanent Inventory

This is a real-time inventory for pharmacies to keep the required separate log of all schedule-II drug transactions.  It is a multi-tenancy application.  Each pharmacy has an *admin* user, and when they create new users, the new user will receive an email with the unique pharmacy ID to register their browser to the pharmacy.
There is a pre-loaded list of common drugs, but each pharmacy can add their own.  It has functions for receiving shipments, doing a required hand balancing of supplies, and a quick POS screen.  The POS screen subtracts from inventory with just two actions: 1) scan the label, enter the quantity and press <enter>.
Since all the data is updated from anywhere in real time, there is no need to ever refresh the screen, nor are there any AJAX processes used.
It worked on mobile devices also.

Every pharmacist I showed it to thought it was great.  All decisions in the pharmacies were made in "corporate".  All contacts to "corporate" when nowhere.  Oh well.
You can see the informational landing page at www.c2logs.com, but there is no current users and no data available in the application at the moment.

## License
Copyright (c) 2015 by Integrity Calculation Systems

This was a project I worked on off and on from 10/2014 - 3/2016.  This repository is using a very old version of Meteor missing a couple proprietary files and is **not** a working version.

This was written in JavaScript and CoffeeScript using [Meteor](https://www.meteor.com/ "Meteor home page").  Meteor is a full-stack isomorphic node.js platform using WebSockets for real-time data updates.  Other pre-processors used are LESS and Jade (now called Pug).

This is a proof of concept that worked, but is no longer active anywhere.  It is definitely not the optimal example of efficient techniques or the right combination of packages.

This uses both Bootstrap-3 and Semantic styles.  (I was leaning toward using Semantic, but many plugins only worked with Bootstrap)

There are a lot of added packages included, including well know ones like JQuery, Underscore, moment, markdown, and tostr.