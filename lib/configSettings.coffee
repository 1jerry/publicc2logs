Accounts.config forbidClientAccountCreation: true

Router.configure
  layoutTemplate: 'basicLayout'
  notFoundTemplate: 'notFound'
  loadingTemplate: 'loading'
  waitOn: ->
    Meteor.subscribe 'stores'  #global subscribe
    Meteor.subscribe 'products'  #global subscribe

if Meteor.isServer
  Accounts.emailTemplates.siteName = "C2Logs"
  Accounts.emailTemplates.from = "C2Logs <us@c2logs.com>"
  Accounts.emailTemplates.enrollAccount.subject = (user) ->
    msg = "Welcome to C2Logs, "
    msg += user.profile?.name or user.username
    msg += ".  "
    msg += "A new store has been created for you" if user.username is "admin"
    msg

  PrettyEmail.options =
    from: 'C2Logs <us@c2logs.com>'
    logoUrl: ''
    companyName: 'C2Logs'
    companyUrl: 'http://c2logs.com'
    companyAddress: '6400 NE Hwy 99, Suite G, Vancouver, WA 98665'
    companyTelephone: '(360) 574-4444'
    companyEmail: 'us@c2logs.com'
    siteName: 'C2Logs Permanent Inventory'
  PrettyEmail.defaults.resetPassword =
    message: "Click to reset your password"
    buttonText: "push me"
