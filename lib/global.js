
Schemas = {};
Collections = {};
UI.registerHelper("Schemas", Schemas);
UI.registerHelper("Collections", Collections);

filteredUserQuery = function(userId, filter, Org) {
    // if not an admin user don't show any other user
    if (!Roles.userIsInRole(userId, ['admin'])) {
        return Meteor.users.find(userId);
    }

    // TODO: configurable limit and pagination
    var queryLimit = 25;
    var my = Meteor.users.findOne(userId), users;
    //noinspection JSUnresolvedVariable
    var thisStore = my.profile.activeStoreId;
    if (Org == 0 && my.globalAdminUser ) thisStore = {$exists:false};

    if(!!filter) {
        // TODO: passing to regex directly could be dangerous
        users = Meteor.users.find({ $and:[{OrgId:thisStore},{
            $or: [
                {'username': {$regex: filter, $options: 'i'}},
                {'profile.name': {$regex: filter, $options: 'i'}},
                {'emails.address': {$regex: filter, $options: 'i'}}
            ]}]
        }, {sort: {emails: 1}, limit: queryLimit});
    } else {
        users = Meteor.users.find({OrgId:thisStore}, {sort: {emails: 1}, limit: queryLimit});
    }
    return users;
};

///// --done--
// 0.2.14 - fix filter on Store Users
// 0.2.13 - put <enter> and ESC logic into all filter inputs
// 0.2.12 - drug details screen.  a) focus on rx; b) skip to qty on <enter>; c) add Back button// 0.2.12 - drug details screen.  a) focus on rx; b) skip to qty on <enter>; c) add Back button
// 0.2.11 - input changes on Drugs screen. a) focus on filter; b) clear with ESC (or click on x); c) <enter> selects first on list
// 0.2.10 - set store from URL and reset store if logged in somewhere else
// 0.2.9.2 - new store selector from stores screen
// 0.2.9 - fix posting transactions from balance
// 0.2.8.2 - fix refresh issue (was setting user's activeStore wrong on code change)
// 0.2.8.1 - fix validation for transaction entry
// 0.2.8 - Receiving works now, even for non-active drugs
// 0.2.7 - fixed all visual aspects of Receiving screen
// 2.6.5 FIXED: store users list doesn't work for ADMIN, until search letter pressed
// FIXED: REFRESH doesn't set store name .  Stores count==0 !?!?
// 2.6 - replace Semantic modal with BS3
// 2.5 new users link (from email) doesn't do anything
// 2.4 Need to select demo or active store
// 3 view balanceRecords from history
// 2 more concise transaction listing
// 1 drugs list errors (show all)
// 19. balance ==> 0.2
// x1. add countedBy date to trxs
// x2. exclude trxs counted in balanced()
// x3. safely set balance dates
// x4. update balance
// x5. change balance screen if started but not finished
// x remove view/productsview; routes/productsRoutes
// x6. show correct balance for store in drugs
// x7. safely write new transactions
// x8. set complete date on countingPeriod record
// x9. error in transactions that prevents add form
/////
// x18 balance screen
// 1. show Add button if no current balance record
// 2. show past list
