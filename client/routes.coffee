#Router.route '/stores', ->
#  this.render 'stores'
#Router.plugin 'dataNotFound',
#  dataNotFoundTemplate: 'notFound'
#
Router.onBeforeAction( ->
  if not Meteor.user()
    @render "notLoggedIn"
  else
    @next()
  return
,
  except: ['store','login']
)


#---------------------------------------------#
#  Router map
#---------------------------------------------#
Router.map( ->
  @route "admin",
    path: "/admin"
    template: "accountsAdmin"
    onBeforeAction: ->
      if Meteor.loggingIn()
        @render @loadingTemplate
      else unless Roles.userIsInRole(Meteor.user(), ["admin"])
        console.log "redirecting"
        @redirect "/drugs"
      @next()
    onAfterAction: ->
      Meta.setTitle('Admin')
    data: ->
      if @ready()
        filter = {}
#        filter = if Session.get 'showGlobal'
#          OrgId:
#            $exists: false
#        else
#          OrgId: Session.get 'activeStoreId'
#        console.log "find users with",filter
        Meteor.users.find filter

  @route 'home',
    path: '/'
    onAfterAction: ->
      Meta.setTitle('')

  @route 'login',
    path: 'login'
    template: 'notLoggedIn'
    onAfterAction: ->
      Meta.setTitle('')

  @route 'balance',
    template: "balanceLog"
    path: "/balance/:_id?"
    waitOn: ->
      Meteor.subscribe 'countingPeriods'
    data: ->
      if @ready() and Meteor.user()
        store = Meteor.user()?.profile.activeStoreId
        query = if @params._id then _id: @params._id else
          store: store
          completedAt:
            $exists: false
        store: store
        doc: Collections.countingPeriods.findOne query
      else
        @render 'notLoggedIn'
    onAfterAction: ->
      Session.set 'abortError', false
      Meta.setTitle('Balance')

  @route 'balancePeriods',
    waitOn: ->
      Meteor.subscribe 'countingPeriods'
    data: ->
      if @ready() and Meteor.user()
        store = Meteor.user()?.profile.activeStoreId
        query =
          store: store
          completedAt:
            $exists: true
        data: Collections.countingPeriods.find query
    onAfterAction: ->
      Meta.setTitle('Balance periods')

  @route 'drugs', ->
    @render 'drugs'
    waitOn: ->
      Meteor.subscribe 'products'
  @route 'drug',
    path: '/drug/:_id',
    data: ->
      Collections.products.findOne
        _id: this.params._id
    waitOn: ->
      Meteor.subscribe 'products'
    onAfterAction: ->
      Meta.setTitle('Drug '+this.params._id)

  @route 'shipments', ->
    @render 'shipments'
  @route 'shipment',
    path: '/shipment/:_id',
    name: "shipment"
    data: ->
      data =
        doc: null
        formType: "insert"
      data.doc = Collections.shipments.findOne @params._id
      Session.set "doc", ""
      if data.doc
        data.formType = "update"
        Session.set "doc", data.doc
      return data
    waitOn: ->
      Meteor.subscribe 'shipments'
    onAfterAction: ->
      Meta.setTitle('Shipment '+this.params._id)

  @route 'stores',
    onBeforeAction: ->
      if @ready()
        if Meteor.user()?.globalUser or Roles.userIsInRole Meteor.user(), ['demo','admin']
          # only allow if a global user
          @next()
        else
          @render 'drugs'
    waitOn: ->
      Meteor.subscribe 'stores'
    data: ->
      if @ready()
        query = {}
        if not Meteor.user()?.globalUser
          query =
            $or: [
              _id: Meteor.user()?.OrgId
            ,
              number: 0
            ]
      stores: Collections.stores.find query
    onAfterAction: ->
      Meta.setTitle('Stores')

  @route 'store',
    path: '/stores/:_id',
    waitOn: ->
      Meteor.subscribe 'stores'
    data: ->
      if @ready()
        localDbg = false
        Meta.setTitle('Store')
        lid = App.id + ".storeId"
        localId = localStorage.getItem lid
        sessionId = Session.get "activeStoreId"
        console.log "setStore: lid, localId, sessionId",lid,localId,sessionId if localDbg
        URLid = @params._id
        res = Collections?.stores?.findOne URLid
        sRec = rRec = lRec = ""
        sChanged = lChanged = false
        console.log "setStore: URLid, res",URLid,res if localDbg
        if res and URLid isnt 'null'
          if URLid isnt sessionId and (
            not Meteor.userId() or
              URLid == localId or
              res.number or
              Meteor.user()?.globalUser or
              not Collections?.stores?.findOne sessionId
          )
            Session.set 'changeStore', URLid
            console.log "Session to set to",URLid if localDbg
            sessionId = URLid
            sChanged = true
          if not Meteor.userId() and res.number and URLid isnt localId
            localStorage.setItem( lid, URLid)
            localId = URLid
            lChanged = true
            console.log "Default store set to",URLid if localDbg
          if URLid isnt localId and res.number and Meteor.user()?.globalUser
            rRec = res
        if sessionId
          sRec = res
          if sessionId isnt URLid
            sRec = Collections?.stores?.findOne sessionId
        Meta.setTitle('Store: '+sRec?.name)
        if localId
          lRec = res
          if localId isnt URLid
            lRec = Collections?.stores?.findOne localId
        rRec:rRec
        sRec:sRec
        lRec:lRec
        sChanged:sChanged
        lChanged:lChanged
)
