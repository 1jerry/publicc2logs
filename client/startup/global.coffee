# Array Remove - By John Resig (MIT Licensed)
Array::remove = (from, to) ->
  rest = @slice((to or from) + 1 or @length)
  @length = (if from < 0 then @length + from else from)
  @push.apply this, rest

Meteor.subscribe 'stores'  #global subscribe


Template.registerHelper "adminUser", ->
  Meteor.user()?.adminUser
Template.registerHelper "globalUser", ->
  Meteor.user()?.globalUser
Template.registerHelper "globalAdminUser", ->
  Meteor.user()?.globalAdminUser
Template.registerHelper "multiStore", ->
  Meteor.user()?.globalUser or Roles.userIsInRole Meteor.user(), ['demo','admin']
Template.registerHelper "allDrugsCount", ->
  Collections.products.find().count()
Template.registerHelper "activeDrugsCount", ->
  query = {}
  key = "stores." + Meteor.user()?.profile?.activeStoreId
  query[key] = {$exists:true}
  Collections.products.find(query).count()

formatDate = (context,options) ->
  fmt = "MM/DD/YYYY"
  fmt += ", HH:mm:ss" if "D" not in options
  if context
    m = moment context
    if "ago" == options
      result = m.fromNow()
    else if "cal" == options
      result = m.calendar()
    else
      result = m.format fmt
  result
App["formatDate"] = formatDate

Template.registerHelper "formatDate", formatDate

keyMonitor = (event) ->
  if event.keyCode > 31 and event.target.localName isnt "input" and event.target.localName isnt "textarea"
    if not (event.ctrlKey or event.metaKey)
      # event.key works in FF & IE, but NOT Chrome
      switch event.keyCode
        when 191
          if event.shiftKey
            $("#helpModal").modal "show"  #?
        when 65 then $("#add-button").click() #A
        when 66 then Router.go('/balance')    #B
        when 68 then Router.go('/drugs')      #D
        when 82 then Router.go('/shipments')  #R
        when 83 then Router.go('/stores')     #S
        when 85 then Router.go('/admin')      #U

Meteor.startup ->
  Meteor.call 'getServerEnv', (err, env)->
    Session.set "appState", env.appState
    App.stateMessage = env.stateMessage
    App.testState = env.testState
    console.log App.stateMessage
  $(window).scroll ->
    pxShow = 90
    fadeInTime = 400
    fadeOutTime = 400
    if $(window).scrollTop() >= pxShow
      if $("#gototop").hasClass('hidden')   and  !$("#gototop").hasClass('animating')
        $("#gototop").transition 'browse in', fadeInTime
    else
      if $("#gototop").hasClass('visible') and  !$("#gototop").hasClass('animating')
        $("#gototop").transition 'browse out', fadeOutTime
  #
  $(document).on 'keyup', keyMonitor
  Meteor.subscribe 'roles'
  Tracker.autorun () ->
    if Meteor.userId()
      x = Session.get("activeStoreId");
      y = Session.get("showGlobal");
      z = if y is true then 0 else x
      Meteor.subscribe 'filteredUsers', Session.get('userFilter'), z
