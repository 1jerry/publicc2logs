#Meteor.startup ->
# Reshape DOM: put back title and meta elements in the head.
# style and script tags can leave in the body tag.
$ '.initial-msg'
  .hide()
$ '.initial-spinner'
  .hide()
$head = $ 'head'
for tag in ['meta', 'title', 'link', 'script' ]
  $tags = $ tag
  $head.append $tags
console.log "coffeeSetup.  IEversion=",IEversion
if window.IEversion
  $('body').append '
  <p class="browser-warning">
    <strong><i class="icon-exclamation-sign"></i> Sorry!</strong> - You are using an <strong>outdated</strong> browser (IE'+IEversion+').<br/>
    This application requires a browser from at least 2010, at least IE10 or other current browser.<br/>
    Please <u><a href="http://browsehappy.com/" target="_blank">upgrade your browser</a></u> to properly view this website.
  </p>
'
