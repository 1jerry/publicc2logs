Template.stores.helpers
  storeCount: ->
    Collections.stores.find().count()
  rtSettings: ->
    collection: @stores
    rowsPerPage: 20
    class: "table table-striped table-hover table-bordered table-condensed pretty pointer"
    showNavigation: "auto"
    showFilter: if Meteor.user()?.globalUser then true else false
    rowClass: (row) ->
      if Session.equals 'activeStoreId', row._id then "selected" else ""
    fields: [
      if Meteor.user()?.adminUser then {
        key: "action"
        label:"Action"
        headerClass: "hidden-print"
        cellClass: "hidden-print"
        sortable: false
        tmpl: Template.ctrl
       }
      else
        key:"blank"
        label:""
        fn: -> ""
        hidden: true
      if Meteor.user()?.globalAdminUser then {
        key: "_id"
        label: "Internal ID"
        headerClass: "hidden-xs"
        cellClass: "hidden-xs"
        }
      else
        key:"blank2"
        label:""
        fn: -> ""
        hidden: true
      "number"
      {
        key:"name"
        label:"Store Name"
        sortOrder:1
      }
      "company"
      "city"
      "state"
      "email"
      {
        key:"createdAt"
        label:"Created At"
        fn: (value) ->
          value.toLocaleString()
      }
      "createdBy"
      {
        key:"drugCount"
        label:"Active"
        fn: (v,o) ->
          query = {}
          key = "stores." + o._id
          query[key] = {$exists:true}
          Collections.products.find(query).count()
      }
    ]

button = ''
Template.stores.events
  "click .reactive-table tbody tr": () ->
    if button == 'modal'
      null
    else  # no button, just selected line
      Session.set 'changeStore', @_id
      Session.set "userFilter", ""
    button = ''


  "click .clear": (event) ->
    el = event.currentTarget.previousElementSibling
    el.value = ""
    $(el).keyup()
    event.preventDefault()

  "keydown .reactive-table-input": (event) ->
    if event.which is 27
      # ESC key should clear input
      event.target.value = ""
      $(event.target).keyup()
      event.preventDefault()
    else if event.which is 13
      # ENTER should open first item
      $('#drugsList tbody tr:first td:first').click()
      event.preventDefault()

Template.stores.rendered = ->
  $(".reactive-table-input").focus()
  $("div.input-group").append('<div class=clear><i class="icon cancel"></i></div>')

Template.store.helpers
  name: ->
    value = @rRec.name
    value += " (" + @rRec.company + ")" if @rRec.company
    value

Template.store.events
  "click #changeDefaultStore": (event,tmpl) ->
    console.log "set default store to",tmpl.data.rRec._id
    lid = App.id + ".storeId"
    localStorage.setItem( lid, tmpl.data.rRec._id)
    Router.go 'drugs'

Template.editStoreModal.rendered = ->

Template.addStoreModal.rendered = ->
  $('input[name=name]').focus()   # not working

AutoForm.addHooks null,
  onError: (name,error) ->
    console.log "AutoForm %s error: %s", name, error

Template.ctrl.events
  "click span[data-toggle='modal']": () ->
    button = "modal"
    Session.set 'thisStore', this

Template.editStoreModal.helpers
  doc: ->
    Session.get 'thisStore'

Template.delStoreModal.helpers
  doc: ->
    Session.get 'thisStore'
  count: () ->
    query = {}
    doc = Session.get 'thisStore'
    if doc
      key = "stores." + doc._id
      query[key] = {$exists:true}
      Collections.products.find(query).count()

Template.delStoreModal.events
  "click #delStoreButton":  ->
    doc = Session.get 'thisStore'
    alert "would delete "+doc._id
    $('#delStore').modal().fadeOut()
    Flash.success "Deleted Store '"+doc.name+"'"

AutoForm.hooks
  addStoreForm:
    before:
      insert: (doc) ->
        doc.IPcreatedFrom = Session.get 'ip'
        doc
    after:
      insert: (error, result) ->
        console.log "TEMP-after, error='%s', result=",error,result
        if not error
          $("#addStore").modal("hide")
          Session.set "changeStore", result if not Meteor.userId()
          store = Collections.stores.findOne(result)
          email = store.email
          Flash.success "An email was sent to <b>" +
            email + "</b> with a link to set the admin password."
          Meteor.call 'sendEmail', 'us+system@c2logs.com', 'us@c2logs.com',
            'A new store was created: ' + store.name,
            'A new store was created in "' + Session.get('appState') + '": "' + store.name + '" from IP: ' + store.IPcreatedFrom
