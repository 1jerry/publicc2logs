Template['drugs'].helpers
  products: ->
    query = {}
    if !Session.get 'seeAll'
      key = "stores." + Meteor.user()?.profile?.activeStoreId
      query[key] = {$exists:true}
    Collections.products.find query
  rtSettings: ->
    id: "drugsList"
    rowsPerPage: 20
    class: "table table-striped table-hover table-bordered table-condensed pretty pointer"
    showNavigation: "auto"
    rowClass: (row) ->
      if !row.active() then 'inactive' else ''
    fields: [
      'NDC'
      {
        key:'desc'
        hidden: yes
      }
      {
        key:'name'
        label:"Full drug name"
        sortOrder:1
        fn : (v,o) ->
          _.join(
            ' '
            _.titleize(o.desc)
            o.dose
            o.form
            if o.pkgSize then '('+ o.pkgSize + ')'
            if o.supplier then '[' + _.titleize(o.supplier) + ']'
          )
      }
      {
        key: 'lastQty'
        label: "Last Verified Qty"
        fn: (v,o) ->
          storeId = Session.get "activeStoreId"
          o.stores?[storeId]
      }
      {
        key: 'curQty'
        label: "Balance"
        fn: (v,o) ->
          if o.active() then o.curQty() || 0 else ""
      }
      {
        key: 'activeToggle'
        label: "Active"
        title: "Click to change"   ##todo
        fn: (v,o) ->
          classString = (if o.active() then 'checkmark green' else 'minus circle red')
          out = '<i class="icon large ' + classString + '"></i>'
          return new Spacebars.SafeString out
      }
      {
        key: 'used'
        label: "Stores"
        headerClass: "hidden-xs"
        cellClass: "hidden-xs"
        fn: (v,o) ->
          _.keys(o.stores or {}).length
      }
      {
        key: 'createdBy'
        label: "Added By"
        headerClass: "hidden-xs"
        cellClass: "hidden-xs"
        fn: (v,o) ->
          number = o.storeNumber
          res = v
          res = _.sprintf "%f - %s", number,v if number >= 0 and v
          return res
      }
    ]

cell = ''
#noinspection CoffeeScriptUnusedLocalSymbols
Template['drugs'].events
  "click .reactive-table tbody tr": (event) ->
    event.stopPropagation()
    $("#inputArea").remove()
    currentOrder.set this
    switch cell
      when 'activeToggle'
        storeId = Session.get "activeStoreId"
        if (@stores?[storeId] or @curQty()) and @active()
          Flash.danger "Cannot deactivate, this NDC has a balance"
        else
          store = 'stores.' + storeId
          obj = {}
          obj[store] = 0
          storeObj = _.clone(@stores) or {}
          storeObj[storeId] = 0
          if @active()
            Collections.products.update _id:@_id,
              $unset: obj
          else
            Collections.products.update _id:@_id,
              $set:
                stores: storeObj
      else
        Router.go('/drug/'+@_id)
    cell = ''

  "click .ui.checkbox": (event) ->
    Session.set('seeAll', 'checked' in event.currentTarget.classList)

  "click .reactive-table td": (event) ->
    cell = @key

  "click button.tgl-btn": (event) ->
    $('#DruginputForm').toggleClass 'hidden'
    event.preventDefault()

  "click .clear": (event) ->
    el = event.currentTarget.previousElementSibling
    el.value = ""
    $(el).keyup()
    event.preventDefault()

  "keydown .reactive-table-input": (event) ->
    if event.which is 27
      # ESC key should clear input
      event.target.value = ""
      $(event.target).keyup()
      event.preventDefault()
    else if event.which is 13
      # ENTER should open first item IF scanned (full NDC matches one line)
      lines = $('#drugsList tbody tr')
      if lines.length is 1
        $('#drugsList tbody tr:first td:first').click()
        event.preventDefault()

Template.drugs.rendered = ->
  $(".reactive-table-input").focus()
  $("div.input-group").append('<div class=clear><i class="icon cancel"></i></div>')

  Meta.setTitle('Drugs')
  self = this
  captionClass = "aligned center ui large blue header"
  text = "Inventory"
  caption = "<caption class='" + captionClass + "'>" + text + "</caption>"
  @$("table#drugsList").prepend caption
  Meteor.setTimeout (->
    self.$("th.activeToggle").attr("title","Click on row to set Active")
    self.$("table").attr("title","Click on row (except Active column) to see/edit transactions. ")
    action = 'uncheck'
    action = 'check' if Session.get 'seeAll'
    $(".ui.checkbox").checkbox(action)
    $("input[name=NDC]").mask("00000-0000-00")
  ), 1000

#noinspection CoffeeScriptUnusedLocalSymbols
AutoForm.hooks
  insertDrugForm:
    before:
      insert: (doc, template) ->
#        if Collections.products.findOne({NDC: doc.NDC})
#          alert "NDC already on file"
#          return false
        doc.desc = _.titleize doc.desc
        doc.stores = {}
        doc.stores[Session.get "activeStoreId"] = 0
        doc
