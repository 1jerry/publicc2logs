Session.set "dataChanged",false
items = []

Tracker.autorun ->
  items = Session.get("doc")?.items or []

Template.shipments.helpers
  collection: ->
    query = {store: Meteor.user()?.profile?.activeStoreId}
    Collections.shipments.find query
  rtSettings: ->
    id: "shipmentsList"
    rowsPerPage: 20
    class: "table table-striped table-hover table-bordered table-condensed pretty pointer"
    showNavigation: "auto"
    rowClass: (row) ->
      "inactive" if row.verifiedBy
    fields: [
      {
        key:'createdAt'
        label:"Created At"
        sortOrder: 1
        fn: (value) ->
          value.toLocaleString()
      }
      {
        key: 'createdBy'
        label: "Created By"
      }
      'invoice'
      'company'
      {
        key: "items"
        label: "NDCs"
        fn: (v,o) ->
          v?.length or 0
      }
      {
        key:'verifiedAt'
        label:"Verified At"
        fn: (value) ->
          value.toLocaleString() if value
      }
      {
        key:'verifiedBy'
        label: "Verified By"
        fn: (v,o) ->
          button = "<button class='ui tiny primary button'>Edit</button>"
          button2 = "<div class='ui tiny red delete button' datasrc='"+o._id+"'>Delete</div>"
          button += button2 unless o.items?.length
          button = new Spacebars.SafeString(button)
          result = if v then v else button

      }
    ]

Template.reactiveTable.rendered = ->
  # first one, in shipments
  captionClass = "aligned center ui large blue header"
  text = "Shipments (click to see/edit)"
  caption = "<caption class='" + captionClass + "'>" + text + "</caption>"
  @$("table#shipmentsList").prepend caption

Template.shipments.events
  "click .reactive-table tbody tr": (event) ->
    event.stopPropagation()
    Router.go 'shipment',
      _id:@_id
    return
  "click .add": ->
    Router.go 'shipment',
      _id:0
  "click .delete.button": (event)->
    event.stopPropagation()
    Collections.shipments.remove(event.currentTarget.getAttribute('datasrc'))

  "click .clear": (event) ->
    el = event.currentTarget.previousElementSibling
    el.value = ""
    $(el).keyup()
    event.preventDefault()

  "keydown .reactive-table-input": (event) ->
    if event.which is 27
      # ESC key should clear input
      event.target.value = ""
      $(event.target).keyup()
      event.preventDefault()
    else if event.which is 13
      # ENTER should open first item
      $('#drugsList tbody tr:first td:first').click()
      event.preventDefault()

Template.shipments.rendered = ->
  $(".reactive-table-input").focus()
  $("div.input-group").append('<div class=clear><i class="icon cancel"></i></div>')
  Meta.setTitle('Shipments')

Template.shipment.events
  "click .reactive-table tbody tr": (event) ->
    if not Session.get('doc').verifiedBy
      addProduct @_id, @NDC
    return false
  "click .back": ->
    Router.go 'shipments'
  "click #update": ->
    doc = Session.get "doc"
    items = doc.items or []
    entered = $('input.quantity')
    items[i].quantity = entered[i].value for item,i in doc.items
    Collections.shipments.update doc._id, {$set:{items:items}}
    Session.set "dataChanged",false
    $("input.quantity").removeClass("modified")
  "click #reset": ->
    Session.set "dataChanged",false
    $("input.quantity").removeClass("modified").each( ->
      @value = @nextElementSibling.value
    )
  "click #finalize": ->
    doc = Session.get "doc"
    console.log "finalize ",doc.invoice
    $("#update")?.click()
    updateDoc =
      verifiedAt: new Date()
      verifiedBy: Meteor.user().username
    console.log "Update shipment data:",updateDoc
    updateTrxs()
    Collections.shipments.update doc._id,{$set:updateDoc}
    Router.go 'drugs'

Template.shipment.helpers
  collection: ->
    Collections.shipments
  notFinal: ->
    not Session.get('doc')?.verifiedBy

Template.shipment.onRendered ->
  title = "Shipment : " + (@data.doc.invoice or "(untitled)")
  title += " (open)" if not Session.get('doc')?.verifiedBy
  Meta.setTitle(title)

Template.selectItems.rendered = ->
  $(".reactive-table-input").focus()
  $("div.input-group").append('<div class=clear><i class="icon cancel"></i></div>')

  self = this
  Meteor.setTimeout (->
    action = 'uncheck'
    action = 'check' if Session.get 'seeAll'
    $(".ui.checkbox").checkbox(action)
    return
  ), 1000

Template.selectItems.events
  "click .remove.button": (event) ->
    doc = Session.get "doc"
    position = _.indexOf(_.pluck(doc.items,"NDC"),@NDC)
    items.remove(position)
    Collections.shipments.update doc._id,{$pull:{items:{id:@id}}}

  "keypress input.quantity, click input.quantity": (event) ->
    $(event.target).removeClass("modified")
    event.stopPropagation() if event.type is 'click'
    oldvalue = event.target.nextElementSibling
    if oldvalue.name isnt "last_quantity"
      console.log "ERROR in Template.selectItems.events: missing hidden last_quantity field"
    else if oldvalue.value isnt event.target.value
      position = _.indexOf(_.pluck(items,"NDC"),@NDC)
      if position >= 0
        console.log "change [%s] to %s",position,event.target.value
        $(event.target).addClass("modified")
        items[position].quantity = event.target.value
        Session.set "dataChanged",true
  "click .ui.checkbox": (event) ->
    Session.set('seeAll', 'checked' in event.currentTarget.classList)
    return
  "click .clear": (event) ->
    el = event.currentTarget.previousElementSibling
    el.value = ""
    $(el).keyup()
    event.preventDefault()

  "keydown .reactive-table-input": (event) ->
    if event.which is 27
      # ESC key should clear input
      event.target.value = ""
      $(event.target).keyup()
      event.preventDefault()
    else if event.which is 13
      # ENTER should select first item
      $('#drugsList tbody tr:first td:first').click()
      event.preventDefault()

drugRecord = ""

Template.selectItems.helpers
  rowClass: ->
    drugRecord = Collections.products.findOne(NDC:@NDC)
    if drugRecord and drugRecord?.active() then "" else "inactive "
  name: ->
    result = drugRecord.desc
    result += " "+drugRecord.dose if drugRecord.dose
    result
  notFinal: ->
    not Session.get('doc')?.verifiedBy
  qtyString: ->
    _.sprintf(
      "%f @ %f/%s = %f"
      @quantity
      drugRecord.pkgSize
      drugRecord.form
      @quantity * drugRecord.pkgSize
    )
  qtyUnit: ->
    drugRecord.pkgSize
  allowDelete: ->
    !Session.get('doc').verifiedBy

  products: ->
    query = {}
    if !Session.get 'seeAll'
      key = "stores." + Meteor.user()?.profile?.activeStoreId
      query[key] = {$exists:true}
    Collections.products.find query
  rtSettings: ->
    rowsPerPage: 20
    class: ->
      result = "table table-striped table-hover table-bordered table-condensed pretty"
      result += " pointer" if not Session.get('doc').verifiedBy

    showNavigation: "auto"
    rowClass: (row) ->
      res = if row.active() then "" else "inactive "
      res += "selected " if row.NDC in _.pluck Session.get("doc").items,'NDC'
      res
    fields: [
      'NDC'
      {
        key:'desc'
        hidden: yes
      }
      {
        key:'name'
        label:"Full drug name"
        sortOrder:1
        fn : (v,o) ->
          _.join(
            ' '
            _.titleize(o.desc)
            o.dose
            o.form
            if o.pkgSize then '('+ o.pkgSize + ')'
            if o.supplier then '[' + _.titleize(o.supplier) + ']'
          )
      }
      {
        key: 'lastQty'
        label: "Last Verified Qty"
        fn: (v,o) ->
          storeId = Session.get "activeStoreId"
          o.stores?[storeId]
      }
      {
        key: 'curQty'
        label: "Balance"
        fn: (v,o) ->
          if o.active() then o.curQty?() || 0 else ""
      }
      {
        key: 'isActive'
        label: "Active"
        fn: (v,o) ->
          classString = (if o.active() then 'checkmark green' else 'minus circle red')
          out = '<i class="icon large ' + classString + '"></i>'
          return new Spacebars.SafeString out
      }
    ]

#noinspection CoffeeScriptUnusedLocalSymbols
AutoForm.hooks
  insertShipment:
#    before:
#      insert: (doc,template) ->
#        doc.store = Meteor.user()?.profile?.activeStoreId
#        console.log "set store ID to ",doc.store
#        doc
    after:
      insert: (error,result) ->
        if error
          console.info("Insert Error for %s:", result)
          Flash.danger error.message
        else
          Router.go('shipment',{_id: result })
#
##################
##################
addProduct = (id,NDC) ->
  doc = Session.get "doc"
  if id in _.pluck doc.items,'id'
    Flash.danger "add","Item already selected"
  else
    newobj =
      id: id
      NDC: NDC
      quantity: 1
    Collections.shipments.update doc._id,{$push:{items:newobj}}
    items.push(newobj)
    console.log "append to items: ",newobj

updateTrxs = ->
  doc = Session.get "doc"
  for line in doc.items
    drug = Collections.products.findOne(line.id)
    newTrx =
      NDC: drug.NDC
      qty: line.quantity * drug.pkgSize
      rx: doc.invoice
      trxDate: new Date()
      type: 'add'
      verifiedAt: new Date()
      verifiedBy: Meteor.user().username
    newTrx.curqty = newTrx.qty + drug.curQty()
    mdate = moment(newTrx.trxDate)
    newTrx.trxDate = mdate.add(mdate.zone(), "m").toDate() #adjust TZ
    console.log "Update '%s' with ",line.NDC,newTrx
    currentOrder.set drug
    x=Collections.transactions.insert newTrx, (error) ->
      if error
        console.log "Error adding transaction: ", error
    console.log "Added Trx ",x
    storeId = Session.get "activeStoreId"
    if !drug.stores or !drug.stores.hasOwnProperty storeId
      stores = drug.stores or {}
      stores =
        stores:stores
      stores.stores[storeId] = 0
      console.log "Activating %s (%s) for store %s: ", drug.NDC, drug._id, storeId, stores
      Collections.products.update drug._id, {$set:stores}, (error) ->
        if error
          console.log "Error updating product: ",error
  return
