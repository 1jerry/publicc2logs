Template['header'].helpers({
    version: App.version,
    stateMessage: function () {return App.stateMessage},
    //appState: function () {
    //    console.log("appState checked")
    //    return  App.appState || "none"
    //},
    company: function() {
        return Session.get("activeStoreName") || "no store"
    },
    dataId: function() {
        return {_id: Session.get( "activeStoreId")}
    },
    demoStore: function () {
        return (
            Session.equals("activeStoreId", Session.get("demoStoreId"))
        )
    },
    globalColor: function() {
        if (Meteor.user() && Meteor.user().globalUser) return 'red'
    }
});

Template['header'].events({
    'click .resize.button' : function () {
        // As long as the new Meteor UI isn't out the whole template will re-render
        $('.header-wrapper').toggleClass('active');
        $('.angle.icon').toggleClass('down').toggleClass('up');
    },
    'click #logoutBtn' : function () {
        Meteor.logout()
        Router.go('drugs')
    },
    'click .add-pharmacy' : function (event) {
        $("#addStore").modal("show");
        event.preventDefault();
    }
});

Template.header.rendered = function () {
    $("td.NDC").mask("00000-0000-00");
};

Template._loginButtonsLoggedInDropdown.rendered = function () {
    $(".dropdown-toggle").prepend($("#userIcon").clone().removeClass("hidden"));
};
