Template.drug.helpers
  color: ->
    available = @curQty()
    switch
      when available < 10 then "red"
      when available < 20 then "orange"
      when available < 30 then "yellow"
      when available > 100 then "green"
      else "teal"

Template.drug.rendered = ->
  $("span.NDC").mask("00000-0000-00")
  currentOrder.set(@data)
  if not @data.active()
    Flash.danger "This drug is not active.  By adding to it here you will activate it."
  $('input[name=rx]').focus()
#  AutoForm.resetForm "insertTrxForm"
  Meta.setTitle(_.truncate(@data.desc,20))


Template.drug.events
  "keydown input[name=rx]": (event) ->
    if event.which is 13
      # move to qty on <enter>
      $('input[name=qty]').focus()
      event.preventDefault()
  "click .back": ->
    Router.go 'drugs'

Template.trxList.helpers
  trxs: ->
    Collections.transactions.find
      NDC: @NDC
      storeId: Session.get "activeStoreId"
#      countedAt:
#        $exists:false
    ,
      sort:
        createdAt: -1
  trxCount: ->
    Collections.transactions.find(
      NDC: @NDC
      storeId: Session.get "activeStoreId"
#      countedAt:
#        $exists:false
      ).count()
  activeDrug: ->
    @active()

Template.insertTrx.helpers
  current_day: ->
    moment().format("YYYY-MM-DD")

Template.footer.events
  "click #gototop": (event) ->
    event.stopPropagation()
    scrollSpeed = 400
    $("html, body").animate {scrollTop:0}, scrollSpeed

#noinspection CoffeeScriptUnusedLocalSymbols
AutoForm.hooks
  insertTrxForm:
    before:
      insert: (doc, template) ->
        doc.NDC = currentOrder.get().NDC or "[missing]"
        doc.dispensed = true if doc.type is 'dispense'
        doc.qty *= -1 if doc.type isnt 'add'
        doc.curqty = currentOrder.get().curQty() + doc.qty
        #noinspection SpellCheckingInspection
        mdate = moment(doc.trxDate)
        doc.trxDate = mdate.add(mdate.zone(), "m").toDate() #adjust TZ
        if not currentOrder.get().active()
          # activate if not active
          storeObj = _.clone(currentOrder.get().stores) or {}
          storeObj[Session.get("activeStoreId")] = 0
          Collections.products.update _id:currentOrder.get()._id,
            $set:
              stores: storeObj
        doc
    after:
      insert: (error,result) ->
        if error
          console.info("Insert Error for NDC %s:", currentOrder.get().NDC, error)
          Flash.danger error.message
        else
          history.back()
          doc = Collections.transactions.findOne(result)
          Flash.success _.sprintf("Item %s adjusted by %f",doc.NDC,doc.qty)
#
