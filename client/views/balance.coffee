drugs = ->
  query = {}
  key = "stores." + Meteor.user()?.profile?.activeStoreId
  query[key] = {$exists:true}
  result = Collections.products.find query, {sort:{desc:1}}
  result

Meteor.setInterval (->
    Session.set 'clock', new Date()
  )
, 1000

Template.balanceLog.helpers
  collection: ->
    Collections.countingPeriods
  items: drugs
  snapshotAge: -> Session.get 'lastCountedAge'
  lastCounted: ->
    x=Session.get 'clock'  # just to make reactive every second
    lc=@doc.lastCounted()
    if lc
      hours = moment(x).diff(lc,'h')
      result = switch
        when hours > 2 then 2
        when hours > 1 then 1
      result
    Session.set 'lastCountedAge', result
    App.formatDate( lc, 'ago')
  countButtonColor: ->
    if Session.get 'lastCountedAge' then 'primary' else 'default'
  startedAt: ->
    x=Session.get 'clock'  # just to make reactive every second
    App.formatDate( @doc.startedAt, 'ago', x)
  allCountsEntered: ->
    Session.get 'IBSrecalc'
    _.all( @doc.items, (line)->
      _.has line, 'packageCount'
    ) and _.all( @doc.items, (line)->
      _.has line, 'extraCount'
    )
  allDone: ->
    Session.get 'IBSrecalc'
    _.all @doc.items, (line)->
      line.calcBalance is 0 or not _.isEmpty line.note
  ready2Finish: ->
    Session.get 'IBSrecalc'
    @doc.counted and @doc.title and not @doc.completedAt and _.all @doc.items, (line)->
      line.calcBalance is 0 or not _.isEmpty line.note
  history: ->
    @doc.verifiedBy and @doc.counted

Template.balanceModal.helpers
  shortCount: (parm) ->
    balances = _.pluck Template.parentData(1).doc.items, 'calcBalance'
    #f = function(last,me) {return me < 0 ? 1+last : last}
    f = (last,me) ->
      if me < 0 then 1+last else last
    if parm is "+"
      f = (last,me) ->
        if me > 0 then 1+last else last
    if parm is 0
      f = (last,me) ->
        if me == 0 then 1+last else last
    _.reduce balances, f, 0

Template.balanceLog.onRendered ->
  if @data?.doc
    title = "Balance: " + (@data.doc.title or "(untitled)")
    title += " (current)" unless @data.doc.verifiedBy
    Meta.setTitle(title)

Template.balanceLog.events
  "click #countButton": (event) ->
    id = event.currentTarget.getAttribute 'data-id'
    Meteor.call 'countNow', id
    event.preventDefault();
  "click #listAll":  ->
    Router.go 'balancePeriods'
  "click #createBatch": ->
      x=drugs().fetch()
      doc =
        items: ({
          id: drug._id
          NDC: drug.NDC
          pkgSize: drug.pkgSize
        } for drug in x )
      id = Collections.countingPeriods.insert doc, (error) ->
        if error
          console.log error
      doc = Collections.countingPeriods.findOne id  # include auto-gen fields


drugRecord = ""

Template.balanceModal.events
  "click #allDone": ->
    id = Template.parentData(0)._id
    Meteor.call 'doneCounting', id
    Router.go 'drugs'
    Meteor.setTimeout (->
      Flash.success "Resetting Inventory Counts.  One Moment Please..."
    ), 500

Template.balanceLine.helpers
  name: ->
    drugRecord = Collections.products.findOne(NDC:@NDC)
    result = drugRecord.desc
    result += " "+drugRecord.dose if drugRecord.dose
    return result
  allDone: ->
    dummy = Tracker.nonreactive ->
      Session.get( 'IBSrecalc') or 0
    Session.set 'IBSrecalc', dummy + 1
    @calcBalance is 0 or not _.isEmpty @note
  readonly: ->
    'readonly' if Template.parentData(1).doc.verifiedBy

Template.balanceLine.events
  'change input': (event) ->
    ele = event.currentTarget
    name = ele.getAttribute 'name'
    lineNum = ele.getAttribute 'data-line-number'
    value = ele.value
    id = Template.parentData(1).doc._id
    method = if name is 'note' then 'logNote' else 'logCount'
    Meteor.call method, id, lineNum, name, value

Template.balancePeriods.helpers
  rtSettings: ->
    id: "balancePeriodsList"
    rowsPerPage: 20
    class: "table table-striped table-hover table-bordered table-condensed pretty pointer"
    showNavigation: "auto"
    fields: [
      'title'
      {
        key:'verifiedAt'
        label:"verified At"
        sortOrder: 1
        fn: (value) ->
          value.toLocaleString()
      }
      {
        key: 'verifiedBy'
        label: "verified By"
      }
      {
        key:'createdAt'
        label:"Created At"
        fn: (value) ->
          value.toLocaleString()
      }
      {
        key: 'createdBy'
        label: "Created By"
      }
      {
        key:'startedAt'
        label:"started At"
        fn: (value) ->
          value.toLocaleString()
      }
      {
        key: 'startedBy'
        label: "started By"
      }
    ]

Template.balancePeriods.events
  "click .reactive-table tbody tr": () ->
    Router.go "balance",
      _id:@_id

  "click .clear": (event) ->
    el = event.currentTarget.previousElementSibling
    el.value = ""
    $(el).keyup()
    event.preventDefault()

  "keydown .reactive-table-input": (event) ->
    if event.which is 27
      # ESC key should clear input
      event.target.value = ""
      $(event.target).keyup()
      event.preventDefault()
    else if event.which is 13
      # ENTER should open first item
      $('#drugsList tbody tr:first td:first').click()
      event.preventDefault()

Template.balancePeriods.rendered = ->
  $(".reactive-table-input").focus()
  $("div.input-group").append('<div class=clear><i class="icon cancel"></i></div>')
