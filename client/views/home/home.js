Template.home.helpers({
  stateMessage: function () {return App.stateMessage},
  appState: function () {
    return  Meteor.settings.state
  },
  'feature' : function () {
    return [
      { 'text' : 'Secure and Immutable', 'icon' : 'archive', 'path' : '#packages' },
      { 'text' : 'Simple to Use', 'icon' : 'terminal', 'path' : '#console-tool' },
      { 'text' : 'Always Accurate', 'icon' : 'html5', 'color' : 'hover-orange', 'path' : '#html5' },
      { 'text' : 'Features', 'icon' : 'folder', 'path' : '#structure' }
    ]
  },
  'title1' : "Secure and Immutable",
  'title2' : "Simple to Use",
  'title3' : "Always Accurate",
  'title4' : "Features",
  'semanticElement' : function () {
    return [
      { 'what' : 'Dispensing Process',
        'withBootstrap' : '1. leave window, move to log. 2. find page for drug.  3. write down numbers and dates.  4. go back to customer.',
        'withSemanticUI' : '1. click or ALT-TAB to C2Log screen.  2. Scan drug, scan Rx #, type dispensed number'
      },
      { 'what' : 'Dispensing Time', 'withBootstrap' : '1-2 minutes', 'withSemanticUI' : '5-10 seconds' },
      { 'what' : 'Adding Inventory Process',
        'withBootstrap' : '1. find page for each drug.  2. write numbers and dates.  3. repeat.',
        'withSemanticUI' : '1. click on Receiving. 2. Scan each drug. 3. type in number. 4. repeat 2 & 3 in any order.'
      },
      { 'what' : 'Adding Inventory (10 items) Time', 'withBootstrap' : '5-15 minutes', 'withSemanticUI' : '1-2 minutes' },
      { 'what' : 'Balancing Inventory Time', 'withBootstrap' : '1-2 HOURS', 'withSemanticUI' : '10-20 minutes' },
      { 'what' : 'Corporate reporting process',
        'withBootstrap' : '1. request pages from most recent balancing.  2. Receive paper or fax hours or days later.  3. enter the information into a spreadsheet.  4. try to make sense of the old data',
        'withSemanticUI' : 'click a couple times.  See LIVE data from REAL time on any or all stores.' },
      { 'what' : 'Corporate reporting time',
        'withBootstrap' : 'hours to days',
        'withSemanticUI' : 'seconds' }

    ];
  },
  'bootstrapCode' : function () {
    return '<div class="btn btn-primary btn-lg"></div>';
  },
  'folder' : function () {
    return []},
  'xfolder' : function () {
    return [
      { 'root' : 'client', 'children' :
        ['compatibility', 'config', ' lib', ' routes', ' startup', ' stylesheets', 'subscriptions',
          'modules', 'views']
      },
      { 'root' : 'model' },
      { 'root' : 'private' },
      { 'root' : 'server', 'children' : ['fixtures', 'lib', 'publications', 'startup'] },
      { 'root' : 'public' },
      { 'root' : 'meteor-boilerplate' }
    ]
  }
});

Template.home.events({
});


Template.home.rendered = function () {
  // @see: http://stackoverflow.com/questions/5284814/jquery-scroll-to-div
  $('a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }

    return true;
  });
};
