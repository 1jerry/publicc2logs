Template.accountsAdmin.helpers({
	users: function() {
		//Meteor.subscribe('filteredUsers', Session.get('userFilter'));
		return Meteor.users.find();
	},

	email: function () {
		if (this.emails && this.emails.length)
			return this.emails[0].address + (!this.emails[0].verified ? "*":"");

		if (this.services) {
			//Iterate through services
			for (var serviceName in this.services) {
				var serviceObject = this.services[serviceName];
				//If an 'id' isset then assume valid service
				if (serviceObject.id) {
					if (serviceObject.email) {
						return serviceObject.email;
					}
				}
			}
		}
		return "";
	},

	searchFilter: function() {
		return Session.get("userFilter");
	},

	myself: function(userId) {
		return Meteor.userId() === userId;
	},

	name: function() {
		res = this.username;
		res += this.mobileUser ? "*" : "";
		res += this.globalUser ? "^" : "";
		return res
	}
});

// search no more than 2 times per second
var setUserFilter = _.throttle(function(template) {
	var search = template.find(".search-input-filter").value;
	Session.set("userFilter", search);
}, 500);

Template.accountsAdmin.events({
	'keyup .search-input-filter': function(event, template) {
        setUserFilter(template);
        return false;
    },

    'click .glyphicon-trash': function(event, template) {
		Session.set('userInScope', this);
    },

    'click .glyphicon-info-sign': function(event, template) {
		Session.set('userInScope', this);
    },

    'click .glyphicon-pencil': function(event, template) {
		$("div.modal .modal-header div.alert").remove();
		Session.set('userInScope', this);
    },

	'click .addNewUser': function(event, template) {
		//event.stopPropagation();
		if ( AutoForm.validateForm("addUserForm")) {
			var fs = event.target.parentNode;
			var options = {
				username: $("input[name=username]", fs).val(),
				email: $("input[name=email]", fs).val(),
				name: $("input[name='name']", fs).val()
			};
			Session.set('newuserInScope', options);
			Meteor.call("addUser", options,
				{_id:Session.get( 'activeStoreId')},
				"",
				Session.get( 'showGlobal'),
				function(error) {
					var message;
					if (!error) {
						AutoForm.resetForm('addUserForm');
						message = _.sprintf(
							"User <b>%s (%s) </b> added.  Email sent to %s."
							, options.username
							, options.name
							, options.email
						);
						Flash.success(message);
					} else {
						event.stopImmediatePropagation();
						var box = $("#newuser");
						$(box).hide();
						message = error.error == '403' ?
							"This email address already used" : error.error;
						Flash.danger( message);
					}
				}
			);
		} else {
			event.stopImmediatePropagation()
		}
	},
	"click .clear": function(event) {
		var el;
		el = event.currentTarget.previousElementSibling;
		el.value = "";
		$(el).keyup();
		return event.preventDefault();
	},
	"keydown .search-input-filter": function(event) {
		if (event.which === 27) {
			event.target.value = "";
			$(event.target).keyup();
			return event.preventDefault();
		}
	},
	"click .ui.checkbox": function(event) {
		var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
		Session.set('showGlobal', __indexOf.call(event.currentTarget.classList, 'checked') >= 0);
	}
});

Template.accountsAdmin.rendered = function() {
	$("div.input-group").append('<div class=clear><i class="icon cancel"></i></div>');
	var searchElement = document.getElementsByClassName('search-input-filter');
	if(!searchElement)
		return;
	var filterValue = Session.get("userFilter");

	var pos = 0;
	if (filterValue)
		pos = filterValue.length;

	searchElement[0].focus();
	searchElement[0].setSelectionRange(pos, pos);
	Session.set('showGlobal', false);
	$(".ui.checkbox").checkbox();

};

Template.newUserModalInner.helpers({
	newuserInScope: function() {
		return Session.get('newuserInScope');
	}

});

AutoForm.hooks({
	addUserForm: {
		before: {
			insert: function(doc, template) {
				return false
			}
		}
	}
});
