Template.notLoggedIn.helpers
  loginErrorMessage: ->
    Template.instance().loginErrorMessage.get()
  username: ->
    Meteor.user().username
  demoStore: ->
    Session.equals 'activeStoreId', Session.get('demoStoreId')
Template.notLoggedIn.onCreated ->
  @loginErrorMessage = new ReactiveVar()
Template.notLoggedIn.events
  'submit form': (e, tmpl) ->
    e.preventDefault()
    user = e.target.loginName.value
    pw = e.target.loginPassword.value
    if pw and user
      Meteor.loginWithPassword username:user, pw, (err) =>
        tmpl.loginErrorMessage.set ""
        if err
          tmpl.loginErrorMessage.set err.reason
        else Router.go 'drugs' if Router.current().route.getName() in ['login','home']
        return
    return
