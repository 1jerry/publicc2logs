localDbg = false

Tracker.autorun () ->
    # for logging in
    id = Meteor.userId()
    sessionStoreId = Session.get "activeStoreId"
    if !id
        if !!(changedStore = Session.get("changeStore"))
            Session.set 'activeStoreId', changedStore
            console.log "Tm3: not logged in, set session store to ",changedStore if localDbg
        else
            console.log "Tm3: logging out" if localDbg
            Session.set("loggedOut",true)
    else
        newStore = false
        if sessionStoreId and Session.get("loggedOut")
            console.log "Tm3: logging %s in.  Set user store:",id,sessionStoreId if localDbg
            Session.set("loggedOut",false)
            newStore = sessionStoreId
        else if !!(changedStore = Session.get("changeStore")) and changedStore isnt sessionStoreId
            newStore = changedStore
            Session.set 'changeStore', ""
        else
            console.log "Tm3: refreshing user %s.  Session store:",id,sessionStoreId if localDbg
        if newStore
            console.log "---Tm3: set user '%s' store:",id,newStore if localDbg
            Meteor.users.update _id: id,
                $set:
                    "profile.activeStoreId": newStore

Tracker.autorun (comp) ->
    # set user's store if the session's store changes
    Session.set('abort',false) or comp.stop() if Session.get('abort')
    activeStoreId = Session.get "activeStoreId"
    sessionStoreId = activeStoreId
    activeStoreId = "" unless Collections.stores?.findOne activeStoreId
    id = Meteor.userId()
    console.log "***** T: Start: user %s, store from Session:",id,activeStoreId,comp.firstRun if localDbg
    if Meteor.user()
        # set session if user and no session
        activeStoreId = Meteor.user().profile?.activeStoreId
        console.log "T: set store from user",activeStoreId if localDbg
    else if not activeStoreId and !id
        # set active store ID
        lid = App.id + ".storeId"
        activeStoreId = localStorage.getItem lid
        activeStoreId = "" unless Collections.stores?.findOne activeStoreId
        console.log "T: get store from localStorage",activeStoreId if localDbg
    if sessionStoreId isnt activeStoreId
        console.log "T: SET Session to",activeStoreId if localDbg
        Session.set 'activeStoreId', activeStoreId
    dummy = Tracker.nonreactive ->
        Meteor.user()
    console.log "T: <<<end>>> user '%s' is now store:",dummy?.username, dummy?.profile.activeStoreId if localDbg

Tracker.autorun () ->
    # for setting store name
    sid = Session.get "activeStoreId"
    store = Collections.stores?.findOne sid
    console.log "Tm1: storeID changed to", sid if localDbg
    if store
        value = store.name
        value += " (" + store.company + ")" if store.company
        console.log "Tm1: SET Session NAME to", value if localDbg
        Session.set 'activeStoreName', value

Tracker.autorun () ->
    Session.get "ip"  # to trigger reset if ever changed
    Meteor.call "getIP", (e, result)  ->
        if not e
#            console.log "IP",result
            Session.set "ip", result

Tracker.autorun (comp) ->
    # for detecting logout event
    if Meteor.user()
        if !App.clearSession
            # only run once
            App.clearSession = true
            console.log "Logged In"
            Meteor.call 'getServerEnv', (err, env)->
                toastr.warning env.stateMessage, "Reminder:"
            thisOrg = Meteor.user().OrgId
            newLogin = thisOrg and not Session.equals "activeStoreId", thisOrg
            if Meteor.user().mobileUser or newLogin
                App.clearSession = Session.get "activeStoreId"
                Session.set 'changeStore', Meteor.user().OrgId
                console.log 'mobileUser or new user: changeStore', Meteor.user().OrgId if localDbg
                Session.set "userFilter", ""
                if newLogin
                    lid = App.id + ".storeId"
                    localStorage.setItem( lid, thisOrg)
                    Router.go 'drugs'
    else if !comp.firstRun
        console.log "Logged Out"
        Session.set 'userFilter', ""
        if App.clearSession and App.clearSession isnt true
            console.log "mobileUser logging out, set store to",App.clearSession if localDbg
            Session.set "changeStore", App.clearSession
        App.clearSession = false
        Router.go 'drugs'

Tracker.autorun () ->
    # for setting demo store ID
    row = Collections.stores?.findOne(
        number: 0
    )
    console.log "Tm2: set demoStoreId if ", row?._id if localDbg
    Session.set "demoStoreId", row._id if row
    sid = Tracker.nonreactive ->
        Session.get "activeStoreId"
    if row and not sid
        console.log "Tm2: SET Session to demo " if localDbg
        Session.set "activeStoreId", row._id

loginOverride = (user, pwd, callback) ->
    console.log "Calling login" if localDbg
    check user, username: String
    activeStoreId = Session.get "activeStoreId"
    console.log "LO-called. user=", user if localDbg
    username = user.username
    newuser = switch
        when _.startsWith username, "!" then username.substr 1
        else activeStoreId + "*" + username
    newuser = username:newuser
    console.log "LO-before. user=", newuser if localDbg
    App.loginWithPassword newuser, pwd, callback

App.loginWithPassword = Meteor.loginWithPassword
Meteor.loginWithPassword = loginOverride

