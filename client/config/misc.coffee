Accounts.ui.config passwordSignupFields: 'USERNAME_ONLY'

Flash.config.timeout = 5000

Meteor.settings ?= {}
meta = Meteor.settings.public?.meta or {}
Meta.config options:
  title: meta.title or 'C2Logs Permanent Inventory'
  suffix: meta.suffix or 'C2Logs'
